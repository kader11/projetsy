<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Mine;


class InfoNainMineController extends AbstractController
{
    /**
     * @Route("/info/nain/mine", name="info_nain_mine")
     */
    public function index()
    {
    	$entityManager = $this->getDoctrine()->getManager();
        $mineRepository = $entityManager->getRepository(Mine::class);
      
 

        return $this->render('info_nain_mine/index.html.twig', [
            'controller_name' => 'InfoNainMineController',
            'mines' => $mineRepository->findAll(),
            'inc'=>0,
        ]);
    }
}

  