<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Nain;
class TailleMoyenneController extends AbstractController
{
    /**
     * @Route("/taille/moyenne", name="taille_moyenne")
     */
    public function index()
      {
    	$entityManager = $this->getDoctrine()->getManager();
         $nainRepository = $entityManager->getRepository(Nain::class);

        return $this->render('taille_moyenne/index.html.twig', [
            'controller_name' => 'TailleMoyenneController',
            'taillemoy'=>$nainRepository->tailleMoyennee($entityManager),
        ]);
    }
}
