<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MineRepository")
 */
class Mine
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="float")
     */
    private $profondeur;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Nain", mappedBy="mine")
     */
    private $nains;

    public function __construct()
    {
        $this->nains = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getProfondeur(): ?float
    {
        return $this->profondeur;
    }

    public function setProfondeur(float $profondeur): self
    {
        $this->profondeur = $profondeur;

        return $this;
    }

    /**
     * @return Collection|Nain[]
     */
    public function getNains(): Collection
    {
        return $this->nains;
    }

    public function addNain(Nain $nain): self
    {
        if (!$this->nains->contains($nain)) {
            $this->nains[] = $nain;
            $nain->setMine($this);
        }

        return $this;
    }

    public function removeNain(Nain $nain): self
    {
        if ($this->nains->contains($nain)) {
            $this->nains->removeElement($nain);
            // set the owning side to null (unless already changed)
            if ($nain->getMine() === $this) {
                $nain->setMine(null);
            }
        }

        return $this;
    }
}
